package pages;

import libraries.Annotations;

public class MyLeadsPage extends Annotations {
	
	public CreateLeadPage clickCreateLead() {
		driver.findElementByLinkText("Create Lead")
		.click();
		return new CreateLeadPage();
		
	}
	public MyLeadsPage clickFindLead() {
		driver.findElementByLinkText("Find Leads")
		.click();
		return this;
		
}
	public MyLeadsPage enterLastName()
	{
		driver.findElementByXPath("//input[@name='lastName']").sendKeys("kabali");
		driver.findElementByClassName("x-btn-text").click();
		return this;
	}
}